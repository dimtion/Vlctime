VLCTime
=======

Proof of concept using VLC3.0 to reproduce an electron app clone of Popcorn Time


<img src="screenshots/film-list2.png" width="300">
<img src="screenshots/film-details2.png" width="300">
<img src="screenshots/playing2.png" width="300">

## Quick start

``` bash
# install dependencies
yarn install
```

Because VLC3 is still in active developement you'll need to download, patch and
install from source VLC. Theses requirements might change _a lot_ before VLC3
is released.

Download VLC3 from git://git.videolan.org/vlc.git
Patch it with the following branch: https://code.videolan.org/dimtion/vlc/commits/torrent-aug17-4
Make, and install VLC3

VLCTime uses the vlce node module to bind with libvlc you need to install it
manually:
```bash
cd node_modules
git pull https://code.videolan.org/dimtion/vlce.git
cd vlce
yarn rebuild
```

Once every dependencies installed, you can launch the developement server:
```bash
# serve with hot reload at localhost:9080
yarn run dev

# build electron application for production
yarn run build

# run unit & end-to-end tests
yarn test

# lint all JS/Vue component files in `src/`
yarn run lint

```

---

## License

VLCTime is Free Software. Released under the [MIT License](LICENSE)
