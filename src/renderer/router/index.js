import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home-page',
      redirect: '/movies'
    },
    {
      path: '/movies',
      name: 'movies',
      component: require('@/components/MoviesPage')
    },
    {
      path: '/movie/:id',
      name: 'movie-details',
      component: require('@/components/MovieDetails'),
      props: (route) => {
        return {
          id: parseInt(route.params.id)
        }
      }
    },
    {
      path: '/search/:query',
      name: 'search',
      component: require('@/components/Search'),
      props: true
    },
    {
      path: '/play/:url',
      name: 'play',
      component: require('@/components/Play'),
      props: true
    }
  ]
})
