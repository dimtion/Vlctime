const state = {
  movies: [
    {
      id: 1,
      title: 'Harry Potter',
      description: 'Rescued from the outrageous neglect of his aunt and uncle, a young boy with a great destiny proves his worth while attending Hogwarts School of Witchcraft and Wizardry.',
      director: 'Chris Columbus',
      stars: 'Daniel Radcliffe, Rupert Grint, Richard Harris',
      imgUrl: 'https://image.tmdb.org/t/p/w1280/dCtFvscYcXQKTNvyyaQr2g2UacJ.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: '/home/dimtion/projects/vlc/vlctime/magnet:?xt=urn:btih:XNZ74P4NWYJYGK7OA7MDW7R3J3VHHVIQ&dn=glass-half-blender-animated-cartoon.mp4&xl=27350743&tr=http%3A%2F%2Fwww.freetorrent.fr%3A55555%2F00000000000000000000000000000000%2Fannounce'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]
    },
    {
      id: 2,
      title: 'The Wolf of Wall Street',
      imgUrl: 'https://image.tmdb.org/t/p/w1280/vK1o5rZGqxyovfIhZyMELhk03wO.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]

    },
    {
      id: 3,
      title: 'The Circle',
      imgUrl: 'https://image.tmdb.org/t/p/w1280/bQVqd5rWrx5GbXhJNuvKy4Viz6j.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]

    },
    {
      id: 4,
      title: 'Mad Max: Fury Road',
      imgUrl: 'https://image.tmdb.org/t/p/w1280/kqjL17yufvn9OVLyXYpvtyrFfak.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]

    },
    {
      id: 5,
      title: 'Les Tuche',
      imgUrl: 'https://image.tmdb.org/t/p/w1280/81ffQyUQbMXNG1GjEr8mRujA9Yx.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]

    },
    {
      id: 6,
      title: 'Agent 237 - Operation Barbershop',
      description: 'Hendrik IJzerbroot – Agent 327 – is a secret agent working for the Netherlands secret service agency. In the twenty comic books that were published since 1968, Martin Lodewijk created a rich universe with international conspiracies, hilarious characters and a healthy dose of Dutch humour.',
      director: 'Blender Foundation',
      stars: 'Unknown',
      imgUrl: 'http://www.cartoonbrew.com/wp-content/uploads/2017/05/barbershop.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]
    },
    {
      id: 7,
      title: 'Elephant Dream',
      description: 'The two main characters, Emo (Cas Jansen) and Proog (Tygo Gernandt), are on a journey in the folds of a giant Machine, exploring the twisted and dark complex of wires, gears and cogs. Until one moment a conflict arises that throws out all their assumptions. This movie short couples lively fun with passionate characters in an epic story line.',
      director: 'Bassam Kurdali',
      stars: 'Cas Jansen, Tygo Gernandt',
      imgUrl: 'https://image.tmdb.org/t/p/w640/HjwEbOrWv12lNg0IPCX543ZDb0.jpg',
      filmUrls: [
        {
          quality: '1080p',
          url: 'magnet:?xt=urn:btih:BSMFSIWQNXGHQSJ7H3SFY2IFYBODW7TI&dn=ed_hd.avi&xl=854537054&tr=http%3A%2F%2Fwww.freetorrent.fr%3A55555%2F00000000000000000000000000000000%2Fannounce'
        }
      ]
    },
    {
      id: 8,
      title: 'Tears of Steel',
      description: 'The film’s premise is about a group of warriors and scientists, who gathered at the “Oude Kerk” in Amsterdam to stage a crucial event from the past, in a desperate attempt to rescue the world from destructive robots.',
      director: 'Ian Hubert',
      stars: 'Derek de Lint, Sergio Hasselbaink',
      imgUrl: 'https://image.tmdb.org/t/p/w640/wY2BTrV3KtzvVwQ2sbjbKYBeLXw.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]
    },
    {
      id: 9,
      title: 'Big Buck Bunny',
      description: 'Follow a day of the life of Big Buck Bunny when he meets three bullying rodents: Frank, Rinky, and Gamera. The rodents amuse themselves by harassing helpless creatures by throwing fruits, nuts and rocks at them. After the deaths of two of Bunny’s favorite butterflies, and an offensive attack on Bunny himself, Bunny sets aside his gentle nature and orchestrates a complex plan for revenge.',
      director: 'Sacha Goedegebure',
      stars: 'None',
      imgUrl: 'https://image.tmdb.org/t/p/w640/uVEFQvFMMsg4e6yb03xOfVsDz4o.jpg',
      filmUrls: [
        {
          quality: '1080p',
          url: 'magnet:?xt=urn:btih:7XDJGXGOMIAKOPFM5YQI5K4ZQZYGZOEM&dn=BigBuckBunny.avi&xl=400455228&tr=http%3A%2F%2Fwww.freetorrent.fr%3A55555%2F00000000000000000000000000000000%2Fannounce'
        },
        {
          quality: '1080pinline',
          url: '/home/dimtion/projects/vlc/films/bbb_sunflower_1080p_30fps_normal.mp4'
        }
      ]
    },
    {
      id: 10,
      title: 'Caminandes: Llamigos',
      description: 'In this episode of the Caminandes cartoon series we learn to know our hero Koro even better! It’s winter in Patagonia, food is getting scarce. Koro the Llama engages with Oti the pesky penguin in an epic fight over that last tasty berry. This short animation film was made with Blender and funded by the subscribers of the Blender Cloud platform. Already since 2007, Blender Institute successfully combines film and media production with improving a free and open source 3D creation pipeline.',
      director: 'Sergey Sharybin',
      stars: 'None',
      imgUrl: 'https://image.tmdb.org/t/p/w640/hTH3pAPIQ9KQkbKJhR9V4zhSamC.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]
    },
    {
      id: 11,
      title: 'Sintel',
      description: 'A wandering warrior finds an unlikely friend in the form of a young dragon. The two develop a close bond, until one day the dragon is snatched away. She then sets out on a relentless quest to reclaim her friend, finding in the end that her quest exacts a far greater price than she had ever imagined.',
      director: 'Colin Levy',
      stars: 'Halina Reijn, Thom Hoffman',
      imgUrl: 'https://image.tmdb.org/t/p/w640/iJQy5tF9eMRU1Id3OqCvimazJZl.jpg',
      filmUrls: [
        {
          quality: '720p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        },
        {
          quality: '1080p',
          url: 'file:///home/dimtion/projects/vlc/wcjs-ugly-demo/The.Expanse.S01E10.FASTSUB.VOSTFR.1080p.WEB-DL.DD5.1.H.264-IMPERIUM.mkv'
        }
      ]
    }
  ]
}

const getters = {
  getMovie: (state) => (id) => {
    return state.movies.find(m => { return m.id === id })
  },
  search: (state) => (query) => {
    return state.movies.filter(m => { return m.title.toLowerCase().indexOf(query.toLowerCase()) !== -1 })
  }
}

const mutations = {}

const actions = {
  someAsyncTask ({ commit }) {
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
